<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('program_share', function (Blueprint $table){
        $table->uuid('id')->primary();
        $table->unsignedBigInteger('rid');
        $table->foreign('rid')->references('id')->on('records')->onDelete('cascade');
        $table->unsignedBigInteger('pid');
        $table->foreign('pid')->references('id')->on('record_programs')->onDelete('cascade');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExits('share');
    }
};
