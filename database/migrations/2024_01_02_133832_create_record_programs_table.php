<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('record_programs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rid');
            $table->foreign('rid')->references('id')->on('records')->onDelete('cascade');
            $table->string('pname',120);
            $table->date('day')->nullable();
            $table->text('question')->nullable();
            $table->text('output')->nullable();
            $table->text('algorithm')->nullable();
            $table->text('program')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('record_programs');
    }
};
