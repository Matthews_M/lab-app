<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordPrograms extends Model
{
    // use HasFactory;
    protected $table = 'record_programs';
    protected $fillable = ['rid','pname','day','question','output','algorithm','program'];
}
