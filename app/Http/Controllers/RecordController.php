<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RecordDetail;
use App\Models\RecordPrograms;
use Illuminate\Http\JsonResponse;

class RecordController extends Controller
{
    private $recordId = null;
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'uid' => 'required',
            'title' => 'required|string|max:255',
            'note' => 'nullable',
        ]);
        RecordDetail::create($validatedData);

        return redirect('dashboard')->with('success', 'Successfull stored');
    }

    public function getrecord()
    {
        $id = auth()->user()->id;
        $data = RecordDetail::where('uid', $id)->get();
        return view('dashboard', ['data' => $data]);
    }

    public function list($id)
    {
        $userid = auth()->user()->id;
        $data = RecordDetail::where('id', $id)->value('uid');
        if ($userid != $data) {
            return abort(404);
        }
        $data = RecordPrograms::where('rid', $id)->get();
        if ($data->isEmpty()) {
            $this->recordId = $id;
            return $this->preInsert();
            // return redirect('editor');
        }
        return view('list', ['data' => $data, 'id' => $id]);
    }
    public function preInsert()
    {
        $rid = $this->recordId;
        if ($rid == null) {
            return redirect('home');
        }
        return view('editor', ['id' => $rid]);
        // public function create_recordprogram(Request $request)
    }
    // public function postInsert($id){
    //     return view('editor')->with('success','');
    // }
    public function insert(Request $request)
    {
        $request->validate([
            'rid' => 'required',
            'pname' => 'required',
        ]);
        $id = $request->rid;
        $newRecord = RecordPrograms::create($request->all());
        return redirect()->route('record.list', ['id' => $id]);
    }

    public function updateFetch($id)
    {
        $data = RecordPrograms::where('id', $id)->get();
        return view('editEditor', ['data' => $data]);
        // return response()->json(['data' => $data]);
    }

    // public function updateProgram(Request $request)
    // {
    //     $request->validate([
    //         'id' => 'required',
    //         'rid' => 'required',
    //         'pname' => 'required',
    //     ]);
    //     $id = $request->rid;
    //     RecordPrograms::where('id', $id)->update($request->except('_token'));
    //     return redirect()->route('record.list', ['id' => $id]);
    // }
    public function updateProgram(Request $request)
{
    $request->validate([
        'id' => 'required',
        'rid' => 'required',
        'pname' => 'required',
    ]);

    try {
        $id = $request->id;
        $rid = $request->rid;

        // Assuming 'id' is the primary key
        RecordPrograms::where('id', $id)->update($request->except(['_token']));

        // return redirect()->route('record.list', ['id' => $rid]);
        return response()->json(['id' => $rid]);
    } catch (\Exception $e) {
        // Log the exception or handle it as needed
        return response()->json(['error' => 'An error occurred. Please try again.'], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }
}

public function destroy($id)
{
    $record = RecordDetail::find($id);

    if (!$record) {
        return abort(404);
    }

    $record->delete();

    return redirect()->route('dashboard');
}

public function destroyProgram($id)
{
    $record = RecordPrograms::find($id);

    if (!$record) {
        return abort(404);
    }
    $rid = RecordPrograms::where('id', $id)->value('rid');
    $record->delete();

    return redirect()->route('record.list', ['id' => $rid]);
}

}
