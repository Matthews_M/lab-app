<x-app-layout>
    <style>
        .jump-box {
            position: absolute;
            top: 35vh;
            left: 35vw;
            transition: ease-in-out;
            display: none
        }
    </style>
    <x-slot name="header">
        <div class="flex justify-between items-center dark:bg-gray-800" >
            <h2 class="font-semibold text-xl text-gray-800 leading-tight dark:text-white">
                {{ __('Dashboard') }}
            </h2>
            <button
                class="text-center  btn  p-2 block bg-blue-500  hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
                onclick="jumpBoxOn()">Create
                New</button>
        </div>
    </x-slot>

    {{-- <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div> --}}
    @if ($data)
    <div class="flex flex-wrap pt-4 justify-between">
        @foreach ($data as $item)
            <div
                class="max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 w-1/3 mt-4 ml-4 mr-4 mb-4">
                <a href="{{ url('list', $item->id) }}">
                    <h5 class="mb-2 text-2xl font-semibold tracking-tight text-gray-900 dark:text-white">
                        {{ $item->title }}</h5>
                </a>
                <p class="mb-3 font-normal text-gray-500 dark:text-gray-400">{{ $item->note }}</p>
                <div class="flex gap-4">
                    <a href="#" class="inline-flex items-center text-blue-600 hover:underline">
                        Edit
                    </a>
                    <a href="#" class="inline-flex items-center text-red-600 hover:underline" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $item->id }}').submit();">
                        Delete
                    </a>
                    <form id="delete-form-{{ $item->id }}" action="{{ route('records.destroy', ['id' => $item->id]) }}" method="post" style="display: none;">
                        @csrf
                        @method('delete')
                    </form>
                </div>
            </div>
        @endforeach
        </div>
    @endif


    <div id="jumpBox" class="jump-box">
        <form action="{{ route('record.store') }}" method="post">
            @csrf
            {{-- <div class="flex-block space-y-4 p-8 max-w-sm rounded overflow-hidden shadow-lg bg-white mt-16 m-auto">
            <input type="hidden" name="uid" value="{{ auth()->user()->id }}">
            <input class=" w-full" type="text" name="title" id="" placeholder="Title">
            <input class=" w-full" type="text" name="note" id="" placeholder="Note">
            <input
                class="text-center w-full btn border-2 p-2 block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
                type="submit" value="Generate">
        </div> --}}
            <div
                class="flex-block space-y-4 p-8 max-w-sm rounded overflow-hidden shadow-lg bg-white mt-16 m-auto relative">
                <button class="absolute top-2 right-3  p-2 rounded-full  focus:outline-none focus:shadow-outline-gray"
                    type="button" onclick="closeForm()">
                    <svg onclick="jumpBoxOff()" class="h-4 w-4 text-gray-600" fill="none" stroke="currentColor"
                        viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12">
                        </path>
                    </svg>
                </button>
                <input type="hidden" name="uid" value="{{ auth()->user()->id }}">
                <input class="w-full" type="text" name="title" placeholder="Title">
                <input class="w-full" type="text" name="note" placeholder="Note">
                <input
                    class="text-center w-full btn border-2 p-2 block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
                    type="submit" value="Generate">
            </div>
        </form>
        <div>
            <script>
                function jumpBoxOn() {
                    var div = document.getElementById("jumpBox");
                    var currentDisplay = div.style.display;
                    div.style.display = currentDisplay === "block" ? "none" : "block";
                }

                function jumpBoxOff() {
                    var div = document.getElementById("jumpBox");
                    var currentDisplay = div.style.display;
                    div.style.display = currentDisplay === "block" ? "none" : "block";
                }
            </script>
</x-app-layout>
