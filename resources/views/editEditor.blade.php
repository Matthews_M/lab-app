<x-app-layout>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <form id="updateForm" action="{{ route('edit.update') }}" method="post">
        @csrf
        @method('PUT')
        <x-slot name="header">
            {{-- {{$data['data'] as item}} --}}
            <div class="flex gap-4">
                <input type="hidden" id="id" name="id" value="{{ $data[0]->id }}">
                <input type="hidden" id="rid" name="rid" value="{{ $data[0]->rid }}">
                <input type="text" id="pname" name="pname" placeholder="Program Name"
                    value="{{ $data[0]->pname }}" required>
                {{-- @if ($data[0]->day == null)
                    <input type="date" name="day" class="w-64" id="today">
                @endif --}}
                <input type="date" id="today" style="display: none">
                <input id="tday" type="date"  name="day" class="w-64" value="{{ $data[0]->day }}">
                <input type="text"value="{{ $data[0]->question }}" id="question" class="w-full" name="question"
                    id="" placeholder="Question">
                {{-- <input class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 cursor-pointer"  type="submit" value="Update"> --}}
                <a id="submitbutton"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 cursor-pointer">Update</a>
            </div>
        </x-slot>
        <style>
            @import url("{{ asset('css/editor.css') }}");
        </style>

        <div class="body-container dark:text-gray-100">
            <div class="output">
                <textarea name="output" id="output1" class="textarea-output p-4 dark:!bg-gray-800" placeholder="Write your Output get">{{ $data[0]->output }}</textarea>
                <!-- <textarea name="" id="output2" class="block textarea-output p-4 " placeholder="Write your algorithm"></textarea> -->
            </div>
            <div class="line"></div>

            <div id="editor-container" class="dark:!bg-gray-800 border border-gray-500">
                <div id="line-numbers" class="dark:!bg-gray-800"></div>
                <textarea name="program" class="dark:!bg-gray-800" id="code-input" placeholder="Type your code here...">{{ $data[0]->program }}</textarea>
            </div>
            <!-- <div id="output-container">
        <h2>Output</h2>
        <div id="output"></div>
      </div> -->
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>

        $(document).ready(function() {
            $("#submitbutton").click(function() {
                var id = $('#id').val();
        var rid = $('#rid').val(); // Assuming rid is the ID of the hidden input field
        var pname = $('#pname').val();
        var day = $('#tday').val();
        var question = $('#question').val();
        var output = $('#output1').val();
        var program = $('#code-input').val();
        var formData = {
            id: id,
            rid: rid,
            pname: pname,
            day: day,
            question: question,
            output: output,
            program: program
            // Add other fields as needed
        };
                // console.log(formData);
                $.ajax({
                    type: "POST",
                    url: "{{ route('edit.update') }}",
                    data: formData,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        // console.log(response);
                        window.location.href = '/list/'+response.id;
                        // Handle success response, if needed
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        console.error(xhr.responseText);
                        // Handle error response, if needed
                    }
                });
            });
        });
    </script>
    <script src="{{ asset('js/editor.js') }}"></script>
</x-app-layout>
