<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center ">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-100 leading-tight">
                {{ __('Dashboard') }}
            </h2>
            <div flex>
                <a href="{{ route('insertprogram', ['id' => $id]) }}"
                    class="text-center  btn  p-2  bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer">
                    New Program</a>
                <a href="{{ route('insertprogram', ['id' => $id]) }}"
                    class="text-center  btn  p-2  bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer">
                    Share</a>
            </div>
        </div>
    </x-slot>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg ">
        <table class="mt-4 w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Question
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Program name
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Action
                    </th>
                    <th scope="col" class="px-6 py-3">

                    </th>
                </tr>
            </thead>
            <tbody>
                {{--  --}}

                @foreach ($data as $item)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row"
                            class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $item->question }}
                        </th>
                        <td class="px-6 py-4">
                            {{ $item->day }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $item->pname }}
                        </td>
                        <td class="px-6 py-4">
                            <a href="{{ route('edit', $item->id) }}"
                                class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit</a> |

                        </td>
                        <td>
                            {{-- <a href="#" class="font-medium text-red-600 dark:text-red-500 hover:underline">Delete</a> --}}
                            <a href="#" class="font-medium text-red-600 dark:text-red-500 hover:underline"
                                onclick="event.preventDefault(); document.getElementById('delete-form-{{ $item->id }}').submit();">
                                Delete
                            </a>
                            <form id="delete-form-{{ $item->id }}"
                                action="{{ route('programs.destroy', ['id' => $item->id]) }}" method="post"
                                style="display: none;">
                                @csrf
                                @method('delete')
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{-- <div
        class="absolute  max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <button class="absolute left-5/6     ">
            <svg onclick="jumpBoxOff()" class="h-4 w-4 text-gray-600" fill="none" stroke="currentColor"
                viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12">
                </path>
            </svg>
        </button>
        <a href="#">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Share Through Link</h5>
        </a>
        <p class="dark:text-gray-300 text-justify mt-4 mb-4">Generate public link. Anyone with access to it can view
            your data without requiring any permissions. Proceed with caution.</p>
        <!-- HTML Structure -->
        <div class="grid grid-cols-8 gap-2 w-full max-w-[23rem]">
            <label for="npm-install" class="sr-only">Label</label>
            <input id="npm-install" type="text"
                class="col-span-6 bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                value="copy text" disabled readonly>
            <button data-copy-to-clipboard-target="npm-install"
                class="col-span-2 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 items-center inline-flex justify-center">
                <span id="default-message">Copy</span>
                <span id="success-message" class="hidden inline-flex items-center">
                    <svg class="w-3 h-3 text-white me-1.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                        fill="none" viewBox="0 0 16 12">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M1 5.917 5.724 10.5 15 1.5" />
                    </svg>
                    Copied!
                </span>
            </button>
        </div>
        <button
            class=" p-4 mt-4 col-span-2 text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full  py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800 items-center inline-flex justify-center"
            onclick="generateLink()">Generate Link</button>
        <!-- JavaScript -->
        <script>
            const button = document.querySelector('[data-copy-to-clipboard-target]');
            const input = document.getElementById('npm-install');
            const defaultMessage = document.getElementById('default-message');
            const successMessage = document.getElementById('success-message');

            button.addEventListener('click', async () => {
                try {
                    await navigator.clipboard.writeText(input.value);
                    // Show the success message
                    successMessage.style.display = 'inline-flex';
                    defaultMessage.style.display = 'none';

                    // Hide the success message after 2 seconds
                    setTimeout(() => {
                        successMessage.style.display = 'none';
                        defaultMessage.style.display = 'inline';
                    }, 2000);
                } catch (err) {
                    console.error('Failed to copy: ', err);
                }
            });
        </script>
        <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
            crossorigin="anonymous"></script>
        <script src="{{ asset('js/list.js') }}">
            //call generate link function
        </script>

    </div> --}}

</x-app-layout>
