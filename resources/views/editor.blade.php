<x-app-layout>
  <!-- action="{{ url('insertProgram') }}" -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <form  action="{{ route('insertProgram') }}" method="post">
    @csrf
    <x-slot name="header">
      <div class="flex gap-4">
        @if ($id)
        <input type="hidden" id="rid" name="rid" value="{{ $id }}">
        @endif
        <input type="text" id="pname" name="pname" placeholder="Program Name" required>
        <input type="date"  name="day" class="w-64" id="today">
        <input type="text" id="question" class="w-full" name="question" id="" placeholder="Question">
        <input class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 cursor-pointer" onclick="insertProgram()" type="submit" value="Save">
      </div>
    </x-slot>
    <style>
      @import url("{{ asset('css/editor.css') }}");
    </style>

    <div class="body-container dark:text-gray-100">
      <div class="output">
        <textarea name="output" id="output1" class="textarea-output p-4 dark:!bg-gray-800" placeholder="Write your Output get"></textarea>
        <!-- <textarea name="" id="output2" class="block textarea-output p-4 " placeholder="Write your algorithm"></textarea> -->
      </div>
      <div class="line"></div>

      <div id="editor-container" class="dark:!bg-gray-800 border border-gray-500">
        <div id="line-numbers" class="dark:!bg-gray-800"></div>
        <textarea class="dark:!bg-gray-800 " name="program" id="code-input" placeholder="Type your code here..."></textarea>
      </div>
      <!-- <div id="output-container">
        <h2>Output</h2>
        <div id="output"></div>
      </div> -->
    </div>
  </form>
  <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
  <script>
    function insertProgram() {
    var rid = $('#rid').val(); // Assuming rid is the ID of the hidden input field
    var pname = $('#pname').val();
    var day = $('#today').val();
    var question = $('#question').val();
    var output = $('#output1').val();
    var program = $('#code-input').val();
    var formData = {
        rid: rid,
        pname: pname,
        day: day,
        question: question,
        output: output,
        program: program
        // Add other fields as needed
    };
      // var formData = $('#insertProgramForm').serialize();
      console.log(formData);
      $.ajax({
        type: 'POST',
        url: '{{ route("insertProgram") }}',
        data: formData,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          // Handle success, e.g., show a success message or redirect
        //   console.log(data);
        window.location.href = "{{ route('record.list', ['id' => $id]) }}";
        },
        error: function(data) {
          // Handle errors, e.g., display an error message
          console.log(data);
        }
      });
    }
  </script>
  <script src="{{ asset('js/editor.js') }}"></script>
</x-app-layout>
