<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RecordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home')->name('home');
});
Route::get('/editor-insert/{id}', function ($id) {
    return view('editor', ['id' => $id]);
})->name('insertprogram');


// Route::get('/edit/{id}', function ($id) {
//     return view('editEditor', ['id'=> $id]);
// })->name('edit');

// Route::get('/editor-insert', [RecordController::class,'postInsert'])->name('editor-insert');

// Route::get('/list', function () {
//     return view('list');
// });
Route::get('/dashboard', [RecordController::class, 'getrecord'])
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::get('/editor', [RecordController::class,'preInsert'])
->middleware(['auth', 'verified'])
->name('editor');
Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    // user defined below
    Route::post('', [RecordController::class, 'store'])->name('record.store');
    Route::get('/list/{id}', [RecordController::class, 'list'])->name('record.list');
    Route::post('/store', [RecordController::class,'insert'])->name('insertProgram');
    Route::get('/edit/{id}', [RecordController::class,'updateFetch'])->name('edit');
    Route::post('/updateEdit', [RecordController::class, 'updateProgram'])->name('edit.update');
    Route::delete('/records/{id}', [RecordController::class, 'destroy'])->name('records.destroy');
    Route::delete('/programs/{id}', [RecordController::class, 'destroyProgram'])->name('programs.destroy');
});

require __DIR__ . '/auth.php';
