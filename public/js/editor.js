const today = new Date().toISOString().split('T')[0];
        document.getElementById('today').value=today;
        const codeInput = document.getElementById('code-input');
        const lineNumbers = document.getElementById('line-numbers');
        const outputContainer = document.getElementById('output-container');
        const output1 = document.getElementById('output1');
        const output2 = document.
    getElementById('output2');

    codeInput.addEventListener('input', updateLineNumbers);

    function updateLineNumbers() {
      const lines = codeInput.value.split('\n');
      const lineNumbersHTML = lines.map((_, index) => `<div>${index + 1}</div>`).join('');
      lineNumbers.innerHTML = lineNumbersHTML;
    }

    // Add event listener for resizing the textareas
    const line = document.querySelector('.line');
    const leftTextarea = output1; // Assuming you want to resize output1
    const rightTextarea = output2; // Assuming you want to resize output2

    let isResizing = false;

    line.addEventListener('mousedown', (event) => {
      isResizing = true;
      document.addEventListener('mousemove', handleMouseMove);
      document.addEventListener('mouseup', () => {
        isResizing = false;
        document.removeEventListener('mousemove', handleMouseMove);
      });
    });

    function handleMouseMove(event) {
      if (isResizing) {
        const newWidth = event.clientX - leftTextarea.getBoundingClientRect().left;
        leftTextarea.style.width = `${newWidth}px`;
        rightTextarea.style.width = `calc(100% - ${newWidth}px)`;
      }
    }
